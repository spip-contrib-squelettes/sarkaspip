<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// =======================================================================================================================================
// Balise : #VERSION_SQUELETTE
// =======================================================================================================================================
// Auteur: SarkASmeL
// Fonction : affiche la version utilise du squelette variable globale $version_squelette
// =======================================================================================================================================
//
/**
 * @param $p
 *
 * @return mixed
 */
function balise_VERSION_SQUELETTE($p) {
	$p->code = 'calcul_version_squelette()';
	$p->interdire_scripts = false;

	return $p;
}

/**
 * @return mixed
 */
function calcul_version_squelette() {
	$informer = chercher_filtre('info_plugin');
	$version = $informer('sarkaspip', 'version');

	return $version;
}

// =======================================================================================================================================
// Balise : #VERSION_PHP
// =======================================================================================================================================
// Auteur: SarkASmeL
// Fonction : affiche la version du PHP utilisee
// =======================================================================================================================================
//
/**
 * @param $p
 *
 * @return mixed
 */
function balise_VERSION_PHP($p) {
	$p->code = 'phpversion()';
	$p->interdire_scripts = false;

	return $p;
}

// =======================================================================================================================================
// Balise : #VISITEURS_CONNECTES
// =======================================================================================================================================
// Auteur: SarkASmeL (base sur le plugin Nombre de visiteurs connectes)
// Fonction : affiche le nombre de visiteurs en cours de connection sur le site
// Parametre: aucun
// =======================================================================================================================================
//
/**
 * @param $p
 *
 * @return mixed
 */
function balise_VISITEURS_CONNECTES($p) {
	$p->code = 'calcul_visiteurs_connectes()';

	return $p;
}

/**
 * @return int|null
 */
function calcul_visiteurs_connectes() {
	$nb = count(preg_files(_DIR_TMP . 'visites/', '.'));

	return $nb;
}

// =======================================================================================================================================
// Balise : #VISITES_SITE
// =======================================================================================================================================
// Auteur: SarkASmeL
// Fonction : affiche le nombre de visites sur le site pour le jour courant, la veille ou depuis le debut
// Parametre: aujourdhui, hier, depuis_debut (ou vide)
// =======================================================================================================================================
//
/**
 * @param $p
 *
 * @return mixed
 */
function balise_VISITES_SITE($p) {
	$jour = interprete_argument_balise(1, $p);
	$jour = isset($jour) ? str_replace('\'', '"', $jour) : '"depuis_debut"';

	$p->code = 'calcul_visites_site(' . $jour . ')';

	return $p;
}

/**
 * @param $j
 *
 * @return int|mixed
 */
function calcul_visites_site($j) {
	$visites = 0;

	if ($j == 'aujourdhui') {
		$auj = date('Y-m-d', strtotime(date('Y-m-d')));
		$select = ['visites'];
		$from = ['spip_visites'];
		$where = ['date=' . sql_quote($auj)];
		$result = sql_select($select, $from, $where);
		if ($row = sql_fetch($result)) {
			$visites = $row['visites'];
		}
	} elseif ($j == 'hier') {
		$hier = date('Y-m-d', strtotime(date('Y-m-d')) - 3600 * 24);
		$select = ['visites'];
		$from = ['spip_visites'];
		$where = ['date=' . sql_quote($hier)];
		$result = sql_select($select, $from, $where);
		if ($row = sql_fetch($result)) {
			$visites = $row['visites'];
		}
	} else {
		$select = ['SUM(visites) AS total_absolu'];
		$from = ['spip_visites'];
		$result = sql_select($select, $from);
		if ($row = sql_fetch($result)) {
			$visites = $row['total_absolu'];
			if ($visites == null) {
				$visites = 0;
			}
		}
	}

	return $visites;
}

// =======================================================================================================================================
// Balise : #AUJOURDHUI
// =======================================================================================================================================
// Auteur: SarkASmeL
// Fonction : retourne la date du jour independamment du contexte d'appel
// =======================================================================================================================================
//
/**
 * @param $p
 *
 * @return mixed
 */
function balise_AUJOURDHUI($p) {
	$p->code = 'date("Y-m-d H:i")';

	return $p;
}

// =======================================================================================================================================
// Balise : #RACINE_SPECIALISEE et BRANCHE_SPECIALISEE
// =======================================================================================================================================
// Auteur: SarkASmeL
// Fonction : retourne la valeur de l'ID de la rubrique demandee ou de toutes les rubriques specialisees sous forme de regex
//            Pour creer une nouvelle rubrique specialisee il suffit de rajouter un mot dans le tableau des mots reserves ($mots_reserves)
// =======================================================================================================================================
//
/**
 * @param $p
 *
 * @return mixed
 */
function balise_RACINE_SPECIALISEE($p) {
	$mot_rubrique = interprete_argument_balise(1, $p);
	$mot_rubrique = isset($mot_rubrique) ? str_replace('\'', '"', $mot_rubrique) : '""';
	$critere = interprete_argument_balise(2, $p);
	$critere = isset($critere) ? str_replace('\'', '"', $critere) : '"in"';
	$mode = "'secteur'";

	$p->code = 'calcul_rubrique_specialisee(' . strtolower($mot_rubrique) . ',' . $mode . ',' . $critere . ')';
	$p->interdire_scripts = false;

	return $p;
}

/**
 * @param $p
 *
 * @return mixed
 */
function balise_BRANCHE_SPECIALISEE($p) {
	$mot_rubrique = interprete_argument_balise(1, $p);
	$mot_rubrique = isset($mot_rubrique) ? str_replace('\'', '"', $mot_rubrique) : '""';
	$critere = interprete_argument_balise(2, $p);
	$critere = isset($critere) ? str_replace('\'', '"', $critere) : '"in"';
	$mode = "'branche'";

	$p->code = 'calcul_rubrique_specialisee(' . strtolower($mot_rubrique) . ',' . $mode . ',' . $critere . ')';
	$p->interdire_scripts = false;

	return $p;
}

/**
 * @param string $mot_rubrique
 * @param string $mode
 * @param string $critere
 *
 * @return string
 */
function calcul_rubrique_specialisee(string $mot_rubrique, string $mode, string $critere) : string {
	// On calcule la liste des mots reserves SarkaSPIP + definis par l'utilisateur
	$mots_reserves = explode(':', _SARKASPIP_MOT_SECTEURS_SPECIALISES);
	if (defined('_PERSO_MOT_SECTEURS_SPECIALISES')) {
		if (_PERSO_MOT_SECTEURS_SPECIALISES !== '') {
			$mots_reserves = array_merge($mots_reserves, explode(':', _PERSO_MOT_SECTEURS_SPECIALISES));
		}
	}
	$types_reserves = explode(':', _SARKASPIP_TYPE_SECTEURS_SPECIALISES);
	if (defined('_PERSO_TYPE_SECTEURS_SPECIALISES')) {
		if (_PERSO_TYPE_SECTEURS_SPECIALISES !== '') {
			$types_reserves = array_merge($types_reserves, explode(':', _PERSO_TYPE_SECTEURS_SPECIALISES));
		}
	}
	$fonds_reserves = explode(':', _SARKASPIP_FOND_SECTEURS_SPECIALISES);
	if (defined('_PERSO_FOND_SECTEURS_SPECIALISES')) {
		if (_PERSO_FOND_SECTEURS_SPECIALISES !== '') {
			$fonds_reserves = array_merge($fonds_reserves, explode(':', _PERSO_FOND_SECTEURS_SPECIALISES));
		}
	}

	// Determination de la liste des mots cles associes aux secteurs specialises demandes par la balise
	$id = '-1';
	$rubriques = [];
	$mots = explode(':', $mot_rubrique);
	if ($critere === 'not_in') {
		$mots = array_diff($mots_reserves, $mots);
		sort($mots);
	}
	if (!$mots[0]) {
		$mots = $mots_reserves;
	}
	// Si on est en en mode secteur (ie. balise #RACINE_SPECIALISEE) et qu'on demande un seul secteur specialise
	// on renvoie une valeur; sinon on renvoie toujours une regexp
	$comparaison_valeur = ($mode === 'secteur') && ($mots[0] === $mot_rubrique);
	// Calcul de la balise
	foreach ($mots_reserves as $_cle => $_valeur) {
		if (in_array($_valeur, $mots)) {
			$rubriques = array_merge($rubriques, calcul_rubrique($_valeur, $types_reserves[$_cle], $fonds_reserves[$_cle], $mode));
		}
	}

	if ($rubriques) {
		$id = implode('|', array_unique($rubriques));
		if (!$comparaison_valeur) {
			$id = '^(' . $id . ')$';
		}
	}

	return $id;
}

/**
 * @param $mot
 * @param $type
 * @param $fond
 * @param $mode
 *
 * @return array
 */
function calcul_rubrique($mot, $type, $fond, $mode = 'rubrique') {
	$id_secteur = 0;
	$id_rubrique = [];
	if (!$mot) {
		return $id_rubrique;
	}

	// On recupere le secteur de base soit via la methode du mot-cle, soit par la config
	if ($type == 'motcle') {
		$select = ['t1.id_objet AS id_rubrique'];
		$from = ['spip_mots_liens AS t1', 'spip_mots AS t2', 'spip_groupes_mots AS t3'];
		$where = ['t3.titre=' . sql_quote('squelette_habillage'),
			't3.id_groupe=t2.id_groupe',
			't2.titre=' . sql_quote($mot),
			't2.id_mot=t1.id_mot',
			't1.objet=' . sql_quote('rubrique'), ];
		$result = sql_select($select, $from, $where);
		if ($row = sql_fetch($result)) {
			$id_secteur = $row['id_rubrique'];
		}
	} elseif ($type == 'config') {
		include_spip('inc/config');
		if (function_exists('lire_config')) {
			$valeur = lire_config($fond . '/rubrique_' . $mot);
			if (($valeur != null) && ($valeur > 0)) {
				$id_secteur = $valeur;
			}
		}
	}

	// Si on est en mode branche on retourne les rubriques de la branche, sinon uniquement le secteur recupere precedemment
	if ($id_secteur) {
		if ($mode == 'branche') {
			$select = ['id_rubrique'];
			$from = ['spip_rubriques AS t1'];
			$where = ['t1.id_secteur=' . sql_quote($id_secteur)];
			$result = sql_select($select, $from, $where);
			while ($row = sql_fetch($result)) {
				if ($row['id_rubrique'] != $id_secteur) {
					$id_rubrique[] = $row['id_rubrique'];
				}
			}
		} else {
			$id_rubrique[] = $id_secteur;
		}
	}

	return $id_rubrique;
}

/*
 * Generer les boutons d'admin des forum selon les droits du visiteur
 *
 * @param object $p
 * @return object
 */

//permettre l'utilisation de comments et sarkaspip (meme si ce n'est pas souhaitable dans cette version de sarkaspip)
if (!defined('_DIR_PLUGIN_COMMENTS')) {
	/**
	 * @param $p
	 *
	 * @return mixed
	 */
	function balise_BOUTONS_ADMIN_FORUM_dist($p) {
		if (($_id = interprete_argument_balise(1, $p)) === null) {
			$_id = champ_sql('id_forum', $p);
		}

		$p->code = "
'<'.'?php
	if (isset(\$GLOBALS[\'visiteur_session\'][\'statut\'])
	  AND \$GLOBALS[\'visiteur_session\'][\'statut\']==\'0minirezo\'
		AND (\$id = '.intval({$_id}).')
		AND	include_spip(\'inc/autoriser\')
		AND autoriser(\'moderer\',\'forum\',\$id)) {
			include_spip(\'inc/actions\');include_spip(\'inc/filtres\');
			echo \"<div class=\'boutons spip-admin actions modererforum\'>\"
			. bouton_action(_T(\'forum:icone_supprimer_message\'),generer_action_auteur(\'instituer_forum\',\$id.\'-off\',ancre_url(self(),\'forum\')),\'poubelle\')
			. bouton_action(_T(\'forum:icone_bruler_message\'),generer_action_auteur(\'instituer_forum\',\$id.\'-spam\',ancre_url(self(),\'forum\')),\'spam\')
			. \"</div>\";
		}
?'.'>'";

		$p->interdire_scripts = false;

		return $p;
	}
}//Comments
