<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-sarkaspip?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sarkaspip_description' => 'Sarka-SPIP es un esqueleto general que ofrece toda la gama de funciones básicas para visualizar los objetos editoriales de SPIP, pero también algunos extras como una agenda, una galería, los foros tipo phpBB, la restricción del acceso (a través del plugin), los sitios favoritos... Es compatible con una serie de complementos que a menudo se integra de forma nativa.
Esta versión se basa en el concepto del esqueleto Z.
Sarka-SPIP está dirigido a todo tipo de usuarios, es de fácil instalación y uso. Para los webmasters, e incluso para los principiantes, es fácil de personalizar a través de su interfaz integrada en el espacio privado de SPIP que modifica el diseño, las avellanas (noisettes), los colores, el formato de página, etc.).',
	'sarkaspip_slogan' => 'Esqueleto de uso general que se ajusta a cualquier situación'
);
