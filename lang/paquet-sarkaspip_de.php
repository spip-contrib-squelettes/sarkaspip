<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-sarkaspip?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'sarkaspip_description' => 'Sarka-SPIP ist ein Vielzweckskelett, welches alle Grundfunktionen zum Anzeigen der SPIP-Objekte enthält und sie durch einige Extras ergänzt. Dazu gehören ein Terminkalender, eine Galerie, Foren im Stil von phpBB, Zugangskontrolle (per Plugin), Lieblingswebsites ... Es ist mit zahlreichen Plugins kompatibel, die es teilweise als Standard enthält.

Diese Version von Sarka-SPIP beruht auf dem Skelettsystem Z.

Sarka richtet sich an alle SPIP-Nutzer*innen und zeichnet sich durch einfache Installation und Benutzung aus. Es ist auch für Anfänger leicht zu steuern, da es eine Verwaltungsoberfläche hat, die in den SPIP-Redaktionsbereich integriert ist (Layout, Plugin Noisettes, Farben, Seitenformate ...).',
	'sarkaspip_slogan' => 'Vielzweckskelett mit zahlreichen EInstellungsmöglichkeiten'
);
