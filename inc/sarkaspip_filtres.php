<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Liste les couleurs atorisées pour la typo.
 *
 * @return array Tableau [nom_couleur] = valeur hexa
 */
function lister_couleurs_typo() : array {
	$couleurs = [];

	include_spip('base/sarkaspip_declarations');
	$config_typo = sarkaspip_declarer_config_typo();
	if (isset($config_typo['couleurs'])) {
		$couleurs = $config_typo['couleurs'];
	}

	return $couleurs;
}

/**
 * Permet de modifier la couleur du texte ou de l'introduction d'un article.
 * Par défaut, ce filtre est appelé dans le pipeline `post_propre`.
 *
 * @example
 *     ```
 *     pour le redacteur : [rouge]xxxxxx[/rouge]
 *     pour le webmaster : [(#TEXTE|typo_couleur)]
 *     ```
 *
 * @param string $texte Texte à colorier
 *
 * @return string Texte colorié
 */
function filtre_typo_couleur_dist(string $texte) : string {
	// Acquérir les valeurs par défaut des couleurs typo
	if ($texte) {
		include_spip('base/sarkaspip_declarations');
		$config_typo = sarkaspip_declarer_config_typo();
		$couleurs_texte = $config_typo['couleurs'];

		if ($couleurs_texte) {
			// Variables personnalisables par l'utilisateur
			// --> Activation (oui) ou desactivation (non) de la fonction
			include_spip('inc/config');
			$typo_couleur_active = (lire_config('sarkaspip_typo/coloration_active', 'non') == 'oui');
			// --> Nuances personnalisables par l'utilisateur
			$couleurs_utilisees = lire_config('sarkaspip_typo/couleurs');

			$recherche = [];
			$remplace = [];
			foreach ($couleurs_texte as $_id_couleur => $_defaut_couleur) {
				$recherche[$_id_couleur] = "/(\[{$_id_couleur}\])(.*?)(\[\/{$_id_couleur}\])/";
				if ($typo_couleur_active) {
					$remplace[$_id_couleur] =
						'<span style="color:' .
						sinon($couleurs_utilisees[$_id_couleur], $_defaut_couleur) .
						';">\\2</span>';
				}
			}
			if (!$remplace) {
				$remplace = '\\2';
			}

			$texte = preg_replace($recherche, $remplace, $texte);
		}
	}

	return $texte;
}

/**
 * Retourne la date-heure de debut de la journee passee en argument.
 *
 * @param string $date Date de la journée
 *
 * @return string
 */
function debut_journee(string $date) : string {
	$jour = '';
	if ($date) {
		$jour = date('d', strtotime($date));
		$mois = date('m', strtotime($date));
		$annee = date('Y', strtotime($date));
		$jour = date('Y-m-d H:i:s', mktime(0, 0, 0, $mois, $jour, $annee));
	}

	return $jour;
}

/**
 * Calcule la date au format demande correspondant au dernier jour du mois precedent celui du timestamp en argument.
 *
 * @param int    $timestamp Le timestamp à partir duquel calculer la date
 * @param string $format    Le format de la date à retourner
 *
 * @return string La date calculée
 */
function fin_mois_precedent(int $timestamp, string $format) : string {
	$date = '';
	if ($timestamp) {
		$date = mktime(0, 0, 0, date('m', $timestamp), 0, date('Y', $timestamp));
		date($format, $date);
	}

	return $date;
}

/**
 * Retourne la date-heure de fin de la journee passee en argument.
 *
 * @param string $date Date de la journée dont on veut l'heure de fin
 *
 * @return string La date calculée à la seconde près
 */
function fin_journee(string $date) : string {
	$jour = '';
	if ($date) {
		$jour = date('d', strtotime($date));
		$mois = date('m', strtotime($date));
		$annee = date('Y', strtotime($date));
		$jour = date('Y-m-d H:i:s', mktime(23, 59, 59, $mois, $jour, $annee));
	}

	return $jour;
}

/**
 * Retourne les blocs d'affichage des tickets par jalon dans la page afaire.
 *
 * @param string $jalons Liste de jalons sous la forme d'une chaine séparée par des ':'
 *
 * @return string HTML de l'affichage de la liste
 */
function afaire_liste_par_jalon(string $jalons) : string {
	$page = '';
	if (
		$jalons
		&& defined('_SARKASPIP_AFAIRE_JALONS_AFFICHES')
	) {
		$liste = explode(':', $jalons);
		$i = 0;
		foreach ($liste as $_jalon) {
			++$i;
			$page .= recuperer_fond(
				'noisettes/afaire/inc_afaire_jalon',
				['jalon' => $_jalon, 'ancre' => 'ancre_jalon_' . (string) $i]
			);
		}
	}

	return $page;
}

/**
 * Retourne les blocs d'affichage des tickets par jalon dans la page afaire.
 *
 * @param string $jalons Liste de jalons sous la forme d'une chaine séparée par des ':'
 *
 * @return string HTML de l'affichage des blocs
 */
function afaire_tdm_par_jalon(string $jalons) : string {
	$page = '';
	if (
		$jalons
		&& defined('_SARKASPIP_AFAIRE_JALONS_AFFICHES')
	) {
		$liste = explode(':', $jalons);
		$i = 0;
		foreach ($liste as $_jalon) {
			++$i;
			$nb = afaire_compteur_jalon($_jalon);
			$nb_str = ($nb === 0) ? _T('sarkaspip:0_ticket') : (($nb === 1) ? (string) $nb . ' ' . _T('sarkaspip:1_ticket') : (string) $nb . ' ' . _T('sarkaspip:n_tickets'));
			$page .= '<li><a href="#ancre_jalon_' . (string) $i . '" title="' . _T('sarkaspip:afaire_aller_jalon') . '">'
				. _T('sarkaspip:afaire_colonne_jalon') . '&nbsp;&#171;&nbsp;' . $_jalon . '&nbsp;&#187;, ' . $nb_str
				. '</a></li>';
		}
	}
	$nb = afaire_compteur_jalon();
	if ($nb > 0) {
		$nb_str = ($nb == 1) ? (string) $nb . ' ' . _T('sarkaspip:1_ticket') : (string) $nb . ' ' . _T('sarkaspip:n_tickets');
		$page .= '<li><a href="#ancre_jalon_non_planifie" title="' . _T('sarkaspip:afaire_aller_jalon') . '">&#171;&nbsp;'
			. _T('sarkaspip:afaire_non_planifies') . '&nbsp;&#187;, ' . $nb_str
			. '</a></li>';
	}

	return $page;
}

/**
 * Retourne le nombre de tickets pour le jalon ou pour le jalon et le statut choisis.
 *
 * @param null|string $jalon  Filtre sur le jalon ou vide pour tous les jalons
 * @param null|string $statut Filtre sur le statut, une liste de statuts séparés par une virgule ou vide pour tous les statuts
 *
 * @return int Nombre de tickets
 */
function afaire_compteur_jalon(?string $jalon = '', ?string $statut = '') : int {
	$from = ['spip_tickets AS t1'];
	$where = [];
	if ($jalon) {
		$where[] = 't1.jalon=' . sql_quote($jalon);
	}
	if ($statut) {
		if (strpos($statut, ',') === false) {
			$where[] = 't1.statut=' . sql_quote($statut);
		} else {
			$where[] = sql_in('t1.statut', explode(',', $statut));
		}
	}

	// Calcul des tickets
	$valeur = sql_countsel($from, $where);
	if ($valeur === false) {
		$valeur = 0;
	}

	return $valeur;
}

/**
 * Retourne le pourcetage de tickets termines sur le nombre de tickets total du jalon.
 *
 * @param null|string $jalon Filtre sur le jalon ou vide pour tous les jalons
 *
 * @return float Le pourcentage de tickets resolus ou fermés
 */
function afaire_avancement_jalon(?string $jalon = '') : float {
	$valeur = 0;

	// Nombre total de tickets pour le jalon
	$n1 = afaire_compteur_jalon($jalon);

	// Nombre de tickets termines pour le jalon
	if ($n1 !== 0) {
		$n2 = afaire_compteur_jalon($jalon, 'resolu,ferme');
		$valeur = floor($n2 * 100 / $n1);
	}

	return $valeur;
}

/**
 * Retourne l'info qu'au moins un ticket a ete cree.
 *
 * @return bool `true` si un ticket existe, `false` sinon.
 */
function afaire_ticket_existe() : bool {
	$existe = false;

	// Test si la table existe
	$table = sql_showtable('spip_tickets', true);
	if ($table) {
		// Nombre total de tickets
		$nb = afaire_compteur_jalon();
		// Nombre de tickets termines pour le jalon
		if ($nb >= 1) {
			$existe = true;
		}
	}

	return $existe;
}

/**
 * Retourne le statut d'un forum, à savoir, non autorise, ouvert, ferme.
 * Pour mémoire, un forum est identifié par son article d'accueil.
 *
 * @param $id_article
 *
 * @return string L'identifiant du statut
 */
function statut_forum($id_article) : string {
	// Par défaut on initialise le statut à non autorisé
	$statut = 'non_autorise';

	if ($id = (int) $id_article) {
		// Forum active ou pas ?
		$where = ['id_article=' . sql_quote($id)];
		$accepter = sql_getfetsel('accepter_forum', 'spip_articles', $where);
		if ($accepter === null) {
			$accepter = 'non';
		}

		// Nombre messages de forum de l'article
		$from = ['spip_forum'];
		$where = [
			'id_objet=' . sql_quote($id),
			'objet=' . sql_quote('article')
		];
		$nb = sql_countsel($from, $where);
		if ($nb === false) {
			$nb = 0;
		}

		// Nombre de tickets termines pour le jalon
		if ($nb >= 1) {
			$statut = ($accepter === 'non') ? 'ferme' : 'ouvert';
		} elseif ($accepter !== 'non') {
			$statut = 'ouvert';
		}
	}

	return $statut;
}

/**
 * Cree les sauvegardes d'une liste de fonds suivant un format et dans un repertoire donne.
 *
 * @param array       $fonds Liste des fonds concernés par la sauvegarde
 * @param string      $ou    Dossier où effectuer la sauvegarde
 * @param null|string $mode  Mode de sauvegarde
 *
 * @return bool true si la sauvegarde s'est bien passée, false sinon
 */
function sauvegarder_fonds(array $fonds, string $ou, ?string $mode = 'maintenance') : bool {
	$ok = true;

	include_spip('inc/config');
	include_spip('inc/flock');
	$dir = $ou;
	foreach ($fonds as $_fond) {
		if ($mode === 'maintenance') {
			$dir = sous_repertoire($ou, $_fond);
			$nom = $_fond . '_' . date('Ymd_Hi') . '.txt';
		} else {
			$nom = $_fond . '.txt';
		}
		$f = $dir . $nom;
		$ok = ecrire_fichier($f, serialize(lire_config($_fond)));
	}

	return $ok;
}

/**
 * Restaure les sauvegardes d'une liste de fonds suivant un format et dans un repertoire donne.
 *
 * @param array $fichiers Liste des fichiers de sauvegarde
 *
 * @return bool true si la restauration s'est bien passée, false sinon
 */
function restaurer_fonds(array $fichiers) : bool {
	$ok = true;

	include_spip('inc/config');
	include_spip('inc/flock');
	foreach ($fichiers as $_fichier) {
		lire_fichier($_fichier, $tableau);
		$fond = basename($_fichier, '.txt');
		$ok = ecrire_config($fond, $tableau);
	}

	return $ok;
}

/**
 * Restaure le titre exact du sujet en supprimant le préfixe éventuel.
 *
 * @param string      $titre  Titre brut du sujet
 * @param null|string $resolu Indicateur de sujet résolu. Prend les valeurs `oui` ou vide pour non.
 *
 * @return string Titre nettoyé
 */
function nettoyer_titre_sujet(string $titre, ?string $resolu = '') : string {
	$titre_nettoye = trim(preg_replace(',^\[(annonce|epingle)](&nbsp;)*,UimsS', '', $titre));
	$titre_nettoye = trim(preg_replace(',_(verrouille|resolu)_,UimsS', '', $titre_nettoye));
	if ($resolu) {
		$titre_nettoye = _T('sarkaspip:titre_sujet_resolu', ['titre' => $titre_nettoye]);
	}

	return $titre_nettoye;
}

/**
 * Détermine si l'inscription est possible pour les visiteurs.
 *
 * @return bool true si l'inscription est autorisee, false sinon
 */
function inscription_possible() : bool {
	// Par défaut l'inscripotion est réputée non autorisée
	$est_autorisee = false;

	// fournir le mode de la config ou tester si l'argument du formulaire est un mode accepte par celle-ci
	include_spip('inc/filtres');
	$mode = tester_config(0, '');

	// pas de formulaire si le mode est interdit
	// pas de formulaire si on a déjà une session avec un statut égal ou meilleur au mode
	if (
		$mode
		and isset($GLOBALS['visiteur_session']['statut'])
		and ($GLOBALS['visiteur_session']['statut'] > $mode)
	) {
		$est_autorisee = true;
	}

	return $est_autorisee;
}

/**
 * Détermine si l'abonnement à une newsletter est possible : nécessite le plugin mailsubscribers.
 *
 * @param string $plugin Préfixe du plugin fournissant la
 *
 * @return bool true si l'abonnement est autorisé, false sinon
 */
function abonnement_possible(string $plugin) : bool {
	$retour = false;

	$informer = chercher_filtre('info_plugin');
	$plugin_actif = ($informer($plugin, 'est_actif') == 1);

	if (
		$plugin_actif
		and (strtolower($plugin) === 'mailsubscribers')
	) {
		$retour = true;
	}

	return $retour;
}

// =======================================================================================================================================
// Filtres : module AGENDA
// =======================================================================================================================================
// Auteur: Smellup
// Fonction : regroupe les filtres permettant les affichages de l'agenda
// =======================================================================================================================================
//
include_spip('inc/sarkaspip_filtres_agenda');
